import { Component, OnInit, Input, EventEmitter, Output, HostListener } from '@angular/core';
import Map from 'ol/Map';
import Tile from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import View from 'ol/View';
import { fromLonLat, transform } from 'ol/proj';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import { Location } from '../../models/Location';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  map: any;
  coordinates: string;
  markerLayer: VectorLayer;

  @Input() debtorsLocation: Location[];
  @Input() killersLocation: Location[];
  @Input() debtorLocation: string;
  @Output() debtorLocationChange = new EventEmitter<string>();
  @Input() killerLocation: string;
 // @Output() selectedLocation = new EventEmitter<Location>();

  constructor() { }

  ngOnInit() {
    this.loadMap();
    this.addKillerMarker(37.41, 8.82);
    if (this.debtorLocation) {
      this.map.updateSize();
      let location = Location.parseLocation(this.debtorLocation);
      this.markerLayer = this.addDebtorMarker(location.latitude, location.longitude);
    }
  }
  ngOnChanges() {
    if (this.debtorsLocation != null && this.killersLocation != null) {
      this.map.updateSize();
        this.addDebtorsMarkers();
        this.addKillersMarkers();
    }
    // if (this.debtorLocation) {
    //   this.map.updateSize();
    //   let location = Location.parseLocation(this.debtorLocation);
    //   this.markerLayer = this.addDebtorMarker(location.latitude, location.longitude);
    // }
    // else if (this.killerLocation) {
    //   this.map.updateSize();
    //   let location = Location.parseLocation(this.killerLocation);
    //   this.markerLayer = this.addKillerMarker(location.latitude, location.longitude);
    // }
  }

  loadMap() {
    this.map = new Map({
      target: 'map',
      layers: [
        new Tile({
          source: new OSM()
        })
      ],
      view: new View({
        center: fromLonLat([37.41, 8.82]),
        zoom: 2
      })
    });
  }

  addDebtorMarker(latitude, longitude) {
    var vectorLayer = new VectorLayer({
      source: new VectorSource({
        features: [new Feature({
          geometry: new Point(fromLonLat([latitude, longitude])),
        })]
      }),
      style: new Style({
        image: new Icon({
          anchor: [0.5, 0.5],
          anchorXUnits: "fraction",
          anchorYUnits: "fraction",
          src: "assets/images/DebtorMarker.png"
        })
      })
    });
    this.map.addLayer(vectorLayer);
    return vectorLayer;
  }

  addKillerMarker(latitude, longitude) {
    var vectorLayer = new VectorLayer({
      source: new VectorSource({
        features: [new Feature({
          geometry: new Point(fromLonLat([latitude, longitude])),
        })]
      }),
      style: new Style({
        image: new Icon({
          anchor: [0.5, 0.5],
          anchorXUnits: "fraction",
          anchorYUnits: "fraction",
          src: "assets/images/KillerMarker.png"
        })
      })
    });
    this.map.addLayer(vectorLayer);
    return vectorLayer;
  }

  addDebtorsMarkers() {
    this.debtorsLocation.forEach(item => {
      this.addDebtorMarker(item.latitude, item.longitude);
    });
  }

  addKillersMarkers() {
    this.killersLocation.forEach(item => {
      this.addKillerMarker(item.latitude, item.longitude);
    });
  }

  zoomToPosition(latitude, longitude) {
    this.map.getView().setCenter(transform([latitude, longitude], 'EPSG:4326', 'EPSG:3857'));
    this.map.getView().setZoom(10);
  }

  onMouseMove(event) {
    this.coordinates = ""
    var lonlat = transform(this.map.getEventCoordinate(event), 'EPSG:3857', 'EPSG:4326');
    this.coordinates += lonlat[0].toFixed(2);
    this.coordinates += "/";
    this.coordinates += lonlat[1].toFixed(2);
  }

  onMouseDown(event) {
    let debtorLocation = new Location();
    console.log(this.coordinates);
    var lonlat = transform(this.map.getEventCoordinate(event), 'EPSG:3857', 'EPSG:4326');
    if (this.markerLayer) {
      this.map.removeLayer(this.markerLayer);
    }
    this.markerLayer = this.addDebtorMarker(lonlat[0], lonlat[1]);
    debtorLocation.latitude = lonlat[0];
    debtorLocation.longitude = lonlat[1];
    this.debtorLocation = debtorLocation.toString();
    this.debtorLocationChange.emit(this.debtorLocation);
  }
}
