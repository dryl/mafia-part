import { Component, OnInit, Inject, Optional } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Debtor } from 'src/models/Debtor';
import { Location } from '../../models/Location';


@Component({
  selector: 'app-debtor-dialog',
  templateUrl: './debtor-dialog.component.html',
  styleUrls: ['./debtor-dialog.component.css']
})
export class DebtorDialogComponent {

  constructor(public dialogRef: MatDialogRef<DebtorDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: Debtor) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // selectedLocation(location: Location) {
  //   this.data.location = location.latitude + "/" + location.longitude;
  // }
}
