import { NgModule } from '@angular/core';
import { DebtorDialogComponent } from './debtor-dialog.component';
import { SharedModule } from 'src/shared/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { MapModule } from '../map/map.module';

@NgModule({
  declarations: [DebtorDialogComponent],
  imports: [
    SharedModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MapModule
  ]
})
export class DebtorDialogModule { }
