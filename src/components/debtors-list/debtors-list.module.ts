import { NgModule } from '@angular/core';
import { SharedModule } from 'src/shared/shared/shared.module';
import { DebtorsListComponent } from './debtors-list.component';
import { DebtorDialogModule } from '../debtor-dialog/debtor-dialog.module';
import { DebtorDialogComponent } from '../debtor-dialog/debtor-dialog.component';
@NgModule({
  declarations: [DebtorsListComponent],
  imports: [
    SharedModule,
    DebtorDialogModule
  ],
  exports: [
    DebtorsListComponent
  ],
  entryComponents: [
    DebtorDialogComponent
  ]
})
export class DebtorsListModule { }
