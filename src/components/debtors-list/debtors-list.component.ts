import { Component, OnInit } from '@angular/core';
import { Debtor } from 'src/models/Debtor';
import { MatDialog } from '@angular/material/dialog';
import { DebtorDialogComponent } from '../debtor-dialog/debtor-dialog.component';

@Component({
  selector: 'app-debtors-list',
  templateUrl: './debtors-list.component.html',
  styleUrls: ['./debtors-list.component.css']
})
export class DebtorsListComponent implements OnInit {

  debtors: Debtor[]; 
  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    this.debtors = [];
    this.debtors.push({id: 0, age: 20, debt: 10000, isTarget: true, location: "17.90/30.54", name: "FirstName0", lastname: "LastName0"});
    this.debtors.push({id: 1, age: 25, debt: 10000, isTarget: true, location: "62.55/31.44", name: "FirstName1", lastname: "LastName1"});
    this.debtors.push({id: 2, age: 40, debt: 10000, isTarget: true, location: "11", name: "FirstName2", lastname: "LastName2"});
    this.debtors.push({id: 3, age: 15, debt: 10000, isTarget: true, location: "15", name: "FirstName3", lastname: "LastName3"});
  }

  openDialog(index: number): void {
    let debtor = this.debtors[index];
    const dialogRef = this.dialog.open(DebtorDialogComponent, {
      width: '600px',
      data: {
        id: debtor.id, 
        name: debtor.name, 
        lastname: debtor.lastname, 
        age: debtor.age,
        debt: debtor.debt,
        isTarget: debtor.isTarget,
        location: debtor.location 
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.debtors[index] = result;
      }
    });
  }
}
