class Position {
top: number;
left: number;
width: number;
height: number;
}
class Options {
    title: string;
}
export class Widget {
   position: Position;
   type: number;
   options: Options;
}