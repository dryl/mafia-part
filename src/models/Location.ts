export class Location {
    latitude: number; 
    longitude: number;

    toString() {
        return this.latitude + "/" + this.longitude;
    }

    static parseLocation(location: string) {
        let parsedLocation = new Location();
        parsedLocation.latitude = Number.parseFloat(location.split('/')[0]);
        parsedLocation.longitude = Number.parseFloat(location.split('/')[1]);
        return parsedLocation;
      }   
}